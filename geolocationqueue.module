<?php

/**
 * @file
 * Contains geolocationqueue.module..
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function geolocationqueue_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the geolocationqueue module.
    case 'help.page.geolocationqueue':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Geo Location Queue Worker') . '</p>';
      return $output;

    default:
  }
}

/**
 * Queue an entity address for geolocation.
 *
 * This is an API helper function only; it is not called from this module.
 *
 * @param EntityInterface $entity
 *   The entity.
 * @param string $field_address
 *   The Drupal field name of the address field.
 * @param string $field_coordinates
 *   The Drupal field name of the coordinates field.
 */
function geolocationqueue_additem(EntityInterface $entity, string $field_address, string $field_coordinates) {
  $item = new stdClass;
  $item->entity_id = $entity->id();
  $item->entity_type = $entity->getEntityTypeId();
  $item->field_address = $field_address;
  $item->field_coordinates = $field_coordinates;

  \Drupal::queue('geolocation')->createItem($item);
}
